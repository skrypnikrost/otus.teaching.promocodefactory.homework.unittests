﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Helpers.Exceptions
{
    public class ExceptionResponse
    {
        public int? Status { get; set; }
        public string Message { get; set; }
    }
}
