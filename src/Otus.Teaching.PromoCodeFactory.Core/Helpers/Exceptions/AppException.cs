﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Helpers.Exceptions
{
    public class AppException : Exception
    {
        public int? StatusCode { get; set; }
        public AppException()
        {

        }

        public AppException(string message, int statusCode)
            : base(message)
        {
            StatusCode = statusCode;
        }

        public AppException(string message, Exception exception)
            : base(message, exception)
        {

        }

        public AppException(string message, int statusCode, Exception exception)
           : base(message, exception)
        {
            StatusCode = statusCode;
        }
    }
}
