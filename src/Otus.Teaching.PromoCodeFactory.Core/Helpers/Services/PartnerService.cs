﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Helpers.DTO;
using Otus.Teaching.PromoCodeFactory.Core.Helpers.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Helpers.Services
{
    public class PartnerService : IPartnerService
    {
        private readonly IRepository<Partner> _partnerRepository;
        private readonly IMapper _mapper;
        private readonly ICurrentDateTimeProvider _currentDateTimeProvider;

        public PartnerService(IRepository<Partner> partnerRepository, IMapper mapper, ICurrentDateTimeProvider currentDateTimeProvider)
        {
            _partnerRepository = partnerRepository;
            _mapper = mapper;
            _currentDateTimeProvider = currentDateTimeProvider;
        }

        public async Task<List<PartnerResponseDTO>> GetPartnersAsync()
        {
            var partners = await _partnerRepository.GetAllAsync();

            return _mapper.Map<List<PartnerResponseDTO>>(partners);
        }

        public async Task CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            var partner = await _partnerRepository.GetByIdAsync(id);

            if (partner == null)
                throw new AppException($"Entity with {id} wasn't found", 500);

            if (!partner.IsActive)
                throw new AppException("Partner is not active", 400);

            //Отключение лимита
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);

            if (activeLimit != null)
            {
                activeLimit.CancelDate = _currentDateTimeProvider.CurrentDateTime;
            }

            await _partnerRepository.UpdateAsync(partner);
        }

        public async Task<PartnerResponseDTO> GetPartnerByIdAsync(Guid id)
        {
            var partner = await _partnerRepository.GetByIdAsync(id);

            if (partner == null)
                throw new AppException($"Entity with {id} wasn't found", 500);

            return _mapper.Map<PartnerResponseDTO>(partner);
        }

        public async Task<PartnerPromoCodeLimitResponseDTO> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            var partner = await _partnerRepository.GetByIdAsync(id);

            if (partner == null)
                throw new AppException($"Entity with {id} wasn't found", 500);

            var limit = partner.PartnerLimits
               .FirstOrDefault(x => x.Id == limitId);

            return _mapper.Map<PartnerPromoCodeLimitResponseDTO>(limit);
        }

        public async Task<Guid> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequestDTO request)
        {
            var partner = await _partnerRepository.GetByIdAsync(id);

            if (partner == null)
                throw new AppException($"Entity with {id} wasn't found", 500);

            //Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
                throw new AppException("Partner is not active", 400);

            if (request.Limit <= 0)
                throw new AppException("Limit must be bigger than 0", 400);

            //Установка лимита партнеру
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);

            if (activeLimit != null)
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;

                //При установке лимита нужно отключить предыдущий лимит
                activeLimit.CancelDate = _currentDateTimeProvider.CurrentDateTime;
            }

            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = request.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.UtcNow,
                EndDate = request.EndDate
            };

            partner.PartnerLimits.Add(newLimit);

            await _partnerRepository.UpdateAsync(partner);

            return newLimit.Id;
        }
    }
}
