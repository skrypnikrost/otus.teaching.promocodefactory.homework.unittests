﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Helpers.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Helpers.Mapping
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Partner, PartnerResponseDTO>();
            CreateMap<PartnerPromoCodeLimit, PartnerPromoCodeLimitResponseDTO>()
                .ForMember(x => x.CreateDate, opt => opt.MapFrom(src => src.CreateDate.ToString("dd.MM.yyyy hh:mm:ss")))
                .ForMember(x => x.CancelDate, opt => opt.MapFrom(src => src.CancelDate.HasValue? src.CancelDate.Value.ToString("dd.MM.yyyy hh:mm:ss") : ""))
                .ForMember(x => x.EndDate, opt => opt.MapFrom(src => src.EndDate.ToString("dd.MM.yyyy hh:mm:ss")));
        }
    }
}
