﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Helpers.DTO
{
    public class SetPartnerPromoCodeLimitRequestDTO
    {
        public DateTime EndDate { get; set; }
        public int Limit { get; set; }
    }
}
