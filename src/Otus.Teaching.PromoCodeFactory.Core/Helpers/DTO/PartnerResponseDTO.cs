﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Helpers.DTO
{
    public class PartnerResponseDTO
    {
        public Guid Id { get; set; }

        public bool IsActive { get; set; }

        public string Name { get; set; }

        public int NumberIssuedPromoCodes { get; set; }

        public List<PartnerPromoCodeLimitResponseDTO> PartnerLimits { get; set; }
    }
}
