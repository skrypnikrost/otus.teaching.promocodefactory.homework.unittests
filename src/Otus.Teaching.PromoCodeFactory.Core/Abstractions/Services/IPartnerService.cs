﻿using Otus.Teaching.PromoCodeFactory.Core.Helpers.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services
{
    public interface IPartnerService
    {
        Task<List<PartnerResponseDTO>> GetPartnersAsync();

        Task<PartnerResponseDTO> GetPartnerByIdAsync(Guid id);

        Task<PartnerPromoCodeLimitResponseDTO> GetPartnerLimitAsync(Guid id, Guid limitId);

        Task<Guid> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequestDTO request);

        Task CancelPartnerPromoCodeLimitAsync(Guid id);
    }
}
