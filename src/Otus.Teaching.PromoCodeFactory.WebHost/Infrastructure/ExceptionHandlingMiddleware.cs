﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Otus.Teaching.PromoCodeFactory.Core.Helpers.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Infrastructure
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                var response = FindException(ex);

                if (response.Status != null)
                {
                    context.Response.StatusCode = response.Status.Value;
                    context.Response.ContentType = "application/json";
                }

                await context.Response.WriteAsync(JsonConvert.SerializeObject(response));
            }
        }

        private ExceptionResponse FindException(Exception ex)
        {
            if (ex is AppException)
            {
                var entityException = ex as AppException;

                return new ExceptionResponse
                {
                    Message = ex.Message,
                    Status = entityException.StatusCode
                };
            }

            return new ExceptionResponse
            {
                Message = ex.Message,
                Status = (int)HttpStatusCode.InternalServerError
            };

        }
    }
}
