﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Helpers.DTO;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Партнеры
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PartnersController
        : ControllerBase
    {
        private readonly IRepository<Partner> _partnersRepository;
        private readonly IPartnerService _partnerService;

        public PartnersController(IRepository<Partner> partnersRepository, IPartnerService partnerService)
        {
            _partnersRepository = partnersRepository;
            _partnerService = partnerService;
        }

        [HttpGet]
        public async Task<ActionResult<List<PartnerResponseDTO>>> GetPartnersAsync()
        {
            var response = await _partnerService.GetPartnersAsync();

            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<List<PartnerResponseDTO>>> GetPartnersAsync(Guid id)
        {
            var response = await _partnerService.GetPartnerByIdAsync(id);

            return Ok(response);
        }

        [HttpGet("{id}/limits/{limitId}")]
        public async Task<ActionResult<PartnerPromoCodeLimitResponseDTO>> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            var response = await _partnerService.GetPartnerLimitAsync(id, limitId);

            return Ok(response);
        }

        [HttpPost("{id}/limits")]
        public async Task<IActionResult> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequestDTO request)
        {
            var newLimitId = await _partnerService.SetPartnerPromoCodeLimitAsync(id, request);
        
            return CreatedAtAction(nameof(GetPartnerLimitAsync), new { id = id, limitId = newLimitId.ToString() }, null);
        }

        [HttpPost("{id}/canceledLimits")]
        public async Task<IActionResult> CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            await _partnerService.CancelPartnerPromoCodeLimitAsync(id);

            return NoContent();
        }
    }
}