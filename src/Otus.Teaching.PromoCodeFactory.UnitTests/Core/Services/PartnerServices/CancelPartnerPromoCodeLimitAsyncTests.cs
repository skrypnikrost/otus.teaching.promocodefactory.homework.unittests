﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Helpers.DTO;
using Otus.Teaching.PromoCodeFactory.Core.Helpers.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Helpers.Services;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Services.PartnerServices
{
    public class CancelPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnerRepositoryMock;
        private readonly IPartnerService _partnerService;
        private readonly Mock<ICurrentDateTimeProvider> _currentDateTimeProvider;
        private readonly IFixture _fixture;

        public CancelPartnerPromoCodeLimitAsyncTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnerRepositoryMock = _fixture.Freeze<Mock<IRepository<Partner>>>();
            _currentDateTimeProvider = _fixture.Freeze<Mock<ICurrentDateTimeProvider>>();
            _partnerService = _fixture.Build<PartnerService>().OmitAutoProperties().Create();
        }

        [Theory]
        [InlineData("def47943-7aaf-44a1-ae21-05aa4948b165")]
        [InlineData("def47943-7aaf-44a1-ae21-05aa4948b225")]
        [InlineData("def47943-7bbf-44a1-ae21-05aa4948b500")]
        public async void CancelPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound(string partnerId)
        {
            // Arrange
            Partner partner = null;

            _partnerRepositoryMock.Setup(repo => repo.GetByIdAsync(Guid.Parse(partnerId)))
                .ReturnsAsync(partner);

            // Act
            Func<Task> result = async () => await _partnerService.CancelPartnerPromoCodeLimitAsync(Guid.Parse(partnerId));

            // Assert
            await result.Should().ThrowAsync<AppException>().WithMessage($"Entity with { partnerId } wasn't found");
        }

        [Fact]
        public async void CancelPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner().PartnerIsBlocked();

            var promocodeLimitRequestDTO = _fixture
                .Build<SetPartnerPromoCodeLimitRequestDTO>()
                .Create();

            _partnerRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            Func<Task> result = async () => await _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, promocodeLimitRequestDTO);

            //// Assert
            await result.Should().ThrowAsync<AppException>().WithMessage($"Partner is not active");
        }
    }
}
