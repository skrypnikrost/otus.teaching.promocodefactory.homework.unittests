﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Helpers.DTO;
using Otus.Teaching.PromoCodeFactory.Core.Helpers.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Helpers.Services;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Services.PartnerServices
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnerRepositoryMock;
        private readonly IPartnerService _partnerService;
        private readonly Mock<ICurrentDateTimeProvider> _currentDateTimeProvider;
        private readonly IFixture _fixture;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnerRepositoryMock = _fixture.Freeze<Mock<IRepository<Partner>>>();
            _currentDateTimeProvider = _fixture.Freeze<Mock<ICurrentDateTimeProvider>>();
            _partnerService = _fixture.Build<PartnerService>().OmitAutoProperties().Create();
        }

        // 1. Если партнер не найден, то также нужно выдать ошибку 404;
        [Theory]
        [InlineData("def47943-7aaf-44a1-ae21-05aa4948b165")]
        [InlineData("def47943-7aaf-44a1-ae21-05aa4948b225")]
        [InlineData("def47943-7bbf-44a1-ae21-05aa4948b500")]
        public async void SetPartnerPromocodeLimitAsync_PartnerIsNotFound_ShouldReturnNotFound(string partnerId)
        {
            // Arrange
            Partner partner = null;

            var promocodeLimitRequestDTO = _fixture.
                Build<SetPartnerPromoCodeLimitRequestDTO>().
                Create();

            _partnerRepositoryMock.Setup(repo => repo.GetByIdAsync(Guid.Parse(partnerId)))
                .ReturnsAsync(partner);

            // Act
            Func<Task> result = async () => await _partnerService.SetPartnerPromoCodeLimitAsync(Guid.Parse(partnerId), promocodeLimitRequestDTO);

            //// Assert
            await result.Should().ThrowAsync<AppException>().WithMessage($"Entity with { partnerId } wasn't found");
        }

        // 2. Если партнер заблокирован, то есть поле IsActive = false в классе Partner, то также нужно выдать ошибку 400;
        [Fact]
        public async void SetPartnerPromocodeLimitAsync_PartnerIsBlocked_ShouldReturnNotFound()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner().PartnerIsBlocked();

            var promocodeLimitRequestDTO = _fixture
                .Build<SetPartnerPromoCodeLimitRequestDTO>()
                .Create();

            _partnerRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            Func<Task> result = async () => await _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, promocodeLimitRequestDTO);

            //// Assert
            await result.Should().ThrowAsync<AppException>().WithMessage($"Partner is not active");
        }

        // 3. Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
        [Fact]
        public async void SetPartnerPromocodeLimitAsync_SetNewLimit_ShouldResetNumberOfPromocode()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner().WithOnlyOneActiveLimit();

            var promocodeLimitRequestDTO = _fixture
                .Build<SetPartnerPromoCodeLimitRequestDTO>()
                .With(x => x.Limit, 2)
                .Create();

            _partnerRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            _partnerRepositoryMock.Setup(repo => repo.UpdateAsync(It.IsAny<Partner>()));

            // Act
            var result = _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, promocodeLimitRequestDTO);

            //// Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        // 3. Если партнеру выставляется лимит, если лимит закончился, то количество не обнуляется;
        [Fact]
        public async void SetPartnerPromocodeLimitAsync_LimitIsFinished_ShouldntResetNumberOfPromocode()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner().WithNotActiveLimit();

            var promocodeLimitRequestDTO = _fixture
                .Build<SetPartnerPromoCodeLimitRequestDTO>()
                .With(x => x.Limit, 2)
                .Create();

            _partnerRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            _partnerRepositoryMock.Setup(repo => repo.UpdateAsync(It.IsAny<Partner>()));

            // Act
            var result = _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, promocodeLimitRequestDTO);

            //// Assert
            partner.NumberIssuedPromoCodes.Should().Be(5);

        }

        //4. При установке лимита нужно отключить предыдущий лимит;

        [Fact]
        public async void SetPartnerPromocodeLimitAsync_SetNewLimit_ShouldTurnOffPreviousLimit()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner().WithOnlyOneActiveLimit();

            var now = new DateTime(2021, 02, 06);

            var promocodeLimitRequestDTO = _fixture
                .Build<SetPartnerPromoCodeLimitRequestDTO>()
                .With(x => x.Limit, 2)
                .Create();

            _currentDateTimeProvider.Setup(x => x.CurrentDateTime)
               .Returns(now);

            _partnerRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            _partnerRepositoryMock.Setup(repo => repo.UpdateAsync(It.IsAny<Partner>()));


            // Act
            var result = _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, promocodeLimitRequestDTO);

            //// Assert
            now.Should().Be(partner.PartnerLimits.ToList()[0].CancelDate.Value);
        }

        //5. Лимит должен быть больше 0, если меньше бросаем ексепшин;
        [Fact]
        public async void SetPartnerPromocodeLimitAsync_LimitLessThanZero_ShouldReturnAppException()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner().PartnerIsBlocked();

            var promocodeLimitRequestDTO = _fixture
                .Build<SetPartnerPromoCodeLimitRequestDTO>()
                .With(x => x.Limit, -3)
                .Create();

            _partnerRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            Func<Task> result = async () => await _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, promocodeLimitRequestDTO);

            //// Assert
            await result.Should().ThrowAsync<AppException>().WithMessage($"Partner is not active");

        }

        // 6. Нужно убедиться, что сохранили новый лимит в базу данных(это нужно проверить Unit-тестом);
        [Fact]
        public async void SetPartnerPromocodeLimitAsync_AddNewLimit_NewLimitShouldBeSavedToDB()
        {
            // Arrange
            var partner = PartnerBuilder.CreateBasePartner().WithOnlyOneActiveLimit();

            var promocodeLimitRequestDTO = _fixture
                .Build<SetPartnerPromoCodeLimitRequestDTO>()
                .With(x => x.Limit, 2)
                .Create();

            _partnerRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            _partnerRepositoryMock.Setup(repo => repo.UpdateAsync(partner));

            // Act
            var result = _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, promocodeLimitRequestDTO);

            //// Assert
            _partnerRepositoryMock.Verify(x => x.UpdateAsync(partner), Times.Once);
        }
    }
}
